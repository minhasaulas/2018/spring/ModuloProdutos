package br.edu.up.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableDiscoveryClient
@SpringBootApplication
public class AppProdutos {

	public static void main(String[] args) {
		SpringApplication.run(AppProdutos.class, args);
	}
}
