package br.edu.up.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.edu.up.app.dominio.Produto;
import br.edu.up.app.dominio.ProdutoVendas;

@RepositoryRestResource(collectionResourceRel = "produtos", path = "produtos", excerptProjection = ProdutoVendas.class)
public interface ProdutoRepository extends CrudRepository<Produto, Long> {

	Produto findByNome(String nome);
}
